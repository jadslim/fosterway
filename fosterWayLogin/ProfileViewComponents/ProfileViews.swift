//
//  ProfileViews.swift
//  fosterWayLogin
//
//  Created by Jad Slim on 2017-08-07.
//  Copyright © 2017 Jad Slim. All rights reserved.
//

import UIKit

class FirstCell: UIView {
    @IBOutlet weak var ProfileImage: UIImageView!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    convenience init()
    {
        self.init()
    }
}

class InputCells: UIView {
    
    @IBOutlet weak var TextFieldTitle: UILabel!
    @IBOutlet weak var Textfield: UITextField!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    convenience init()
    {
        self.init()
    }
}
class SixthCell: UIView {

    
    @IBOutlet weak var ClassYearLabel: UILabel!
    @IBOutlet weak var ClassYearStepper: UIStepper!
    @IBOutlet weak var HighSchoolTextField: UIButton!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    convenience init()
    {
        self.init()
    }

    
}
class SeventhCell: UIView {
    @IBOutlet weak var GenderToggle: UISegmentedControl!
    @IBOutlet weak var LanguageTogle: UISegmentedControl!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    convenience init()
    {
        self.init()
    }
}
class EigthCell: UIView {
    @IBOutlet weak var StatusTextField: UIButton!
    @IBOutlet weak var PhoneTextField: UITextField!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    convenience init()
    {
        self.init()
    }
}
class NinthCell: UIView {

    @IBOutlet weak var SummaryTextView: UITextView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    convenience init()
    {
        self.init()
    }
}

