import UIKit
import Firebase
import SnapKit
import UIKit
import SnapKit
import FirebaseStorage
import SwiftyJSON
import ActionSheetPicker_3_0
import FirebaseAuthUI

class EditProfileViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, schoolDelegate {
    func passSchool(school: String?) {
        print(school)
        self.SixthCellInit.HighSchoolTextField.setTitle(school, for: .normal)
    
    }
    
    var ref: DatabaseReference!
    var activity = UIActivityIndicatorView()
    var school: String?
    let scrollView  = UIScrollView()
    var placeholderLabel = UILabel()
    let picker = UIImagePickerController()
    let defaults = UserDefaults.standard
    var profileImage: UIImage?
    var profileUrl: String?
    let FirstCellInit = Bundle.loadView(fromNib: "FirstCell", withType: FirstCell.self)
    let SecondCellInit = Bundle.loadView(fromNib: "InputCells", withType: InputCells.self)
    let ThirdCellInit = Bundle.loadView(fromNib: "InputCells", withType: InputCells.self)
    let FourthCellInit = Bundle.loadView(fromNib: "InputCells", withType: InputCells.self)
    let FifthCellInit = Bundle.loadView(fromNib: "InputCells", withType: InputCells.self)
    let SixthCellInit = Bundle.loadView(fromNib: "SixthCell", withType: SixthCell.self)
    let SeventhCellInit = Bundle.loadView(fromNib: "SeventhCell", withType: SeventhCell.self)
    let EighthCellInit = Bundle.loadView(fromNib: "EigthCell", withType: EigthCell.self)
    let NinthCellInit = Bundle.loadView(fromNib: "NinthCell", withType: NinthCell.self)
    
    var InputStacks: [InputCells]?
    var ProfileEditRef: DatabaseReference!
    var cameFrom = String()
    
    func addSchool(){
        if cameFrom == "school" {
            if let school = school {
                SixthCellInit.HighSchoolTextField.setTitle(school, for: .normal)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        observeProfileData()
        InputStacks = [SecondCellInit, ThirdCellInit, FourthCellInit, FifthCellInit]
        self.customizingCells()
        self.navigationBarConfig()
        self.hideKeyboardWhenTappedAround()
        self.updateViewConstraint()
        NinthCellInit.SummaryTextView.delegate = self
        textViewPlaceHolderAttributes()
        ProfileEditRef = Database.database().reference()
        SixthCellInit.ClassYearStepper.maximumValue = 1
        SixthCellInit.ClassYearStepper.minimumValue = -1
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.delegate = self
        FirstCellInit.ProfileImage.contentMode = UIViewContentMode.scaleAspectFill
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    func updateViewConstraint() {
        view.addSubview(scrollView)
        //adding and customizing First Cell
        FirstCellInit.ProfileImage.image = #imageLiteral(resourceName: "logo")
        roundedProfileImage(image: FirstCellInit.ProfileImage)
        //First Cell Components
        let stackView = UIStackView(arrangedSubviews: [FirstCellInit, SecondCellInit,ThirdCellInit,FourthCellInit,FifthCellInit,SixthCellInit, SeventhCellInit, EighthCellInit, NinthCellInit])
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .fill
        stackView.spacing = 5
        stackView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(stackView)
        
        scrollView.snp.makeConstraints { make in
            make.top.equalTo(self.topLayoutGuide.snp.bottom)
            make.trailing.leading.bottom.equalToSuperview()
        }
        stackView.snp.makeConstraints { make in
            make.edges.equalTo(scrollView)
            make.width.equalTo(scrollView)
        }
        FirstCellInit.snp.makeConstraints { (make) in
            make.height.equalTo(130)
        }
        InputStacks?.forEach({ (stack) in
            stack.snp.makeConstraints({ (make) in
                make.height.equalTo(60)
            })
        })
        SixthCellInit.snp.makeConstraints{ (make) in
            make.height.equalTo(60)
        }
        SeventhCellInit.snp.makeConstraints{ (make) in
            make.height.equalTo(140)
        }
        EighthCellInit.snp.makeConstraints{ (make) in
            make.height.equalTo(60)
        }
        NinthCellInit.snp.makeConstraints { (make) in
            make.height.equalTo(160)
        }
        self.activityIndicatorLayout()
    }
    func customizingCells(){
        SecondCellInit.TextFieldTitle.text = "First Name"
        SecondCellInit.Textfield.setBottomBorder()
        ThirdCellInit.TextFieldTitle.text = "Last Name"
        ThirdCellInit.Textfield.setBottomBorder()
        FourthCellInit.TextFieldTitle.text = "City"
        FourthCellInit.Textfield.setBottomBorder()
        FifthCellInit.TextFieldTitle.text = "Province/State"
        EighthCellInit.PhoneTextField.setBottomBorder()
        InputStacks?.forEach({ (stack) in
            stack.Textfield.setBottomBorder()
        })
        
    }
    func roundedProfileImage(image: UIImageView){
        image.layer.borderWidth = 1
        image.layer.masksToBounds = false
        image.layer.borderColor = UIColor.black.cgColor
        image.layer.cornerRadius = image.frame.height/2
        image.clipsToBounds = true
    }
    func navigationBarConfig(){
        self.navigationItem.title = "Edit Profile"
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(save))
        
        if cameFrom == "home" {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Sign Out", style: .plain, target: self, action: #selector(signOut))
        self.navigationItem.setHidesBackButton(true, animated:true)
        } else {
            self.navigationItem.setHidesBackButton(false, animated:true)
        }
 
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func textViewPlaceHolderAttributes(){
        placeholderLabel.text = "Enter some text..."
        placeholderLabel.font = UIFont.italicSystemFont(ofSize: (NinthCellInit.SummaryTextView.font?.pointSize)!)
        placeholderLabel.sizeToFit()
        NinthCellInit.SummaryTextView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (NinthCellInit.SummaryTextView.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        PlaceHolderOnLoad()
    }
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    func PlaceHolderOnLoad() {
        if (!self.NinthCellInit.SummaryTextView.text.isEmpty) {
            placeholderLabel.isHidden = true
        } else {
            placeholderLabel.isHidden = false
        }
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scrollView.contentInset = UIEdgeInsets.zero
        } else {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    @objc func save(){
        saveInFirebase(firstName: self.SecondCellInit.Textfield.text, lastName: self.ThirdCellInit.Textfield.text)
    }
    @IBAction func yearStepper(_ sender: UIStepper) {
        
        SixthCellInit.ClassYearLabel.text = String(Int(SixthCellInit.ClassYearLabel.text!)! + Int(sender.value))
        SixthCellInit.ClassYearStepper.value = 0
    }
    
    func saveInFirebase(firstName: String?, lastName: String?) {
        startActivity()
        if let first = firstName, let last = lastName, !first.isEmpty || !last.isEmpty  {
            
            let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
            changeRequest?.displayName = first + " " + last
            changeRequest?.commitChanges(completion: nil)
            
            let storageRef = Storage.storage().reference()
            if let image = profileImage {
                var data = Data()
                data = UIImageJPEGRepresentation(image, 0.8)!
                // Create a reference to the file you want to upload
                let ProfileRef = storageRef.child("profileImages/profile\((defaults.string(forKey: "uid"))!).jpg")
                // Upload the file to the path "images/rivers.jpg"
                let uploadTask = ProfileRef.putData(data, metadata: nil) { (metadata, error) in
                    guard let metadata = metadata else {
                        // Uh-oh, an error occurred!
                        self.stopActivity()
                        self.alert(message: (error?.localizedDescription)!)
                        return
                    }
                    if (error != nil) {
                        self.stopActivity()
                        self.alert(message: (error?.localizedDescription)!)
                        return
                    }
                    if let profileUrl = metadata.downloadURL()?.absoluteString {
                        self.profileUrl = profileUrl
                    } else {
                        self.profileUrl = ""
                    }
                    self.uploadData(firstName: first, lastName: last, city: self.FourthCellInit.Textfield.text, province: self.FifthCellInit.Textfield.text, highSchool: self.SixthCellInit.HighSchoolTextField.currentTitle, year: self.SixthCellInit.ClassYearLabel.text, gender: self.SeventhCellInit.GenderToggle.titleForSegment(at: self.SeventhCellInit.GenderToggle.selectedSegmentIndex), language: self.SeventhCellInit.LanguageTogle.titleForSegment(at: self.SeventhCellInit.LanguageTogle.selectedSegmentIndex), status: self.EighthCellInit.StatusTextField.currentTitle, phoneNumber: self.EighthCellInit.PhoneTextField.text, summary: self.NinthCellInit.SummaryTextView.text, profileUrl: self.profileUrl)
                }
            } else {
                uploadData(firstName: first, lastName: last, city: FourthCellInit.Textfield.text, province: FifthCellInit.Textfield.text, highSchool: SixthCellInit.HighSchoolTextField.currentTitle, year: SixthCellInit.ClassYearLabel.text, gender: SeventhCellInit.GenderToggle.titleForSegment(at: SeventhCellInit.GenderToggle.selectedSegmentIndex ), language: SeventhCellInit.LanguageTogle.titleForSegment(at: SeventhCellInit.LanguageTogle.selectedSegmentIndex), status: EighthCellInit.StatusTextField.currentTitle, phoneNumber: EighthCellInit.PhoneTextField.text, summary: NinthCellInit.SummaryTextView.text, profileUrl: "")
            }
            
        } else {
            stopActivity()
            alert(message: "Please Fill First Name and Last Name")
            return
        }
    }

    func uploadData(firstName: String?, lastName: String?, city: String?, province: String?, highSchool: String?, year: String?, gender: String?, language: String?, status: String?, phoneNumber: String?, summary: String?, profileUrl: String?){
        let ref = self.ProfileEditRef.child("user").child((defaults.string(forKey: "uid"))!).child("ProfileData")
        let data = ["First Name": firstName,
                    "Last Name": lastName,
                    "City": city,
                    "Province": province,
                    "High School": highSchool,
                    "Class Year": year,
                    "gender" : gender,
                    "Primary Language": language,
                    "Status": status,
                    "profileUrl": self.profileUrl,
                    "Phone Number": phoneNumber,
                    "Summary": summary]
        ref.updateChildValues(data, withCompletionBlock: { (error, ref) in
            if (error != nil) {
                self.stopActivity()
                self.alert(message: (error?.localizedDescription)!)
            } else {
            self.approveEdit()
            self.stopActivity()
            }
        })
    }
    @IBAction func PickProfileImage(_ sender: Any) {
        self.present(picker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            FirstCellInit.ProfileImage.image = image
            self.profileImage = image
        } else{
            alert(message: "could not fetch image")
        }
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func approveEdit() {
        ref.child("user").child((defaults.string(forKey: "uid"))!).child("EditProfile").updateChildValues(["status": true]) { (error, dataref) in
            if (error == nil) {
                print("success")
                self.navigationController?.popViewController(animated: true)
            } else {
                self.alert(message: "there was an error")
                print("there was an error")
                print(error?.localizedDescription)
            }
        }
    }
    @objc func signOut() {
        try! Auth.auth().signOut()
        self.navigationController?.popToRootViewController(animated: true)
    }
    func activityIndicatorLayout(){
        self.view.addSubview(activity)
        activity.backgroundColor = UIColor.lightGray
        activity.snp.makeConstraints { (make) in
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(self.view.snp.height)
            make.centerX.equalTo(self.view.snp.centerX)
            make.centerY.equalTo(self.view.snp.centerY)
        }
    }
    
    func startActivity(){
        activity.startAnimating()
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
    }
    
    func stopActivity() {
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
        self.activity.stopAnimating()
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        }
    }
    func observeProfileData() {
        var firstName = String()
        var lastName = String()
        
        ref.child("user").child(defaults.string(forKey: "uid")!).child("ProfileData").observe(DataEventType.value, with: { (snapshot) in
            print(snapshot.value)
            let json = JSON((snapshot.value))
            if let FirstName = json["First Name"].string, let LastName = json["Last Name"].string {
                firstName = FirstName
                lastName = LastName
                self.SecondCellInit.Textfield.text = firstName
                self.ThirdCellInit.Textfield.text = lastName
            }
            if let HighSchool = json["High School"].string {
                if (!HighSchool.isEmpty) {
                    self.SixthCellInit.HighSchoolTextField.setTitle(HighSchool, for: .normal)//currentTitle = HighSchool
                }
            }
            if let city = json["City"].string {
                if (!city.isEmpty) {
                    self.FourthCellInit.Textfield.text = city
                }
            }
            if let Province = json["Province"].string {
                if (!Province.isEmpty) {
                    self.FifthCellInit.Textfield.text = Province
                }
            }
            if let gender = json["gender"].string {
                if (!gender.isEmpty) {
                    if gender == "Male" {
                        self.SeventhCellInit.GenderToggle.selectedSegmentIndex = 1
                    } else if gender == "Female" {
                        self.SeventhCellInit.GenderToggle.selectedSegmentIndex = 0
                    }
                }
            }
            if let language = json["Primary Language"].string {
                if (!language.isEmpty) {
                    if language == "English" {
                        self.SeventhCellInit.LanguageTogle.selectedSegmentIndex = 0
                    } else if language == "French" {
                        self.SeventhCellInit.LanguageTogle.selectedSegmentIndex = 1
                    } else {
                        self.SeventhCellInit.LanguageTogle.selectedSegmentIndex = 2
                    }
                }
            }
            if let ClassYear = json["Class Year"].string {
                self.SixthCellInit.ClassYearLabel.text = ClassYear
            }
            if let Status = json["Status"].string {
                if (!Status.isEmpty) {
                    self.EighthCellInit.StatusTextField.setTitle(Status, for: .normal)
                } else {
                    self.EighthCellInit.StatusTextField.setTitle("Researching", for: .normal)
                }
            }
            if let Phone = json["Phone Number"].string {
                if (!Phone.isEmpty) {
                    self.EighthCellInit.PhoneTextField.text = Phone
                }
            }
            if let Summary = json["Summary"].string {
                if (!Summary.isEmpty) {
                    self.NinthCellInit.SummaryTextView.text = Summary
                    self.PlaceHolderOnLoad()
                } else {
                    self.PlaceHolderOnLoad()
                }
            }
            if let ProfileImage = json["profileUrl"].string {
                if (!ProfileImage.isEmpty) {
                    self.profileUrl = ProfileImage
                    self.FirstCellInit.ProfileImage.kf.indicatorType = .activity
                    self.FirstCellInit.ProfileImage.kf.setImage(with: URL(string:ProfileImage))
                }
            }
        }) { (error) in
            self.alert(message: error.localizedDescription)
        }
    }
    @IBAction func ChooseStatus(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Status", rows: ["Researching", "Accepted", "Applying", "Committed"], initialSelection: 1, doneBlock: {
            picker, value, index in
            self.EighthCellInit.StatusTextField.setTitle(index as! String, for: .normal)
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    @IBAction func chooseSchool(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "schooltable") as! SchoolsTableController
        controller.delegate = self
        self.show(controller, sender: nil)
    }
}
