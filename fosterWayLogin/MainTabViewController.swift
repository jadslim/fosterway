//
//  MainTabViewController.swift
//  fosterWayLogin
//
//  Created by Jad Slim on 2017-08-08.
//  Copyright © 2017 Jad Slim. All rights reserved.
//

import UIKit
import Firebase

class MainTabViewController: UITabBarController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.hidesBackButton = true
    }
}
