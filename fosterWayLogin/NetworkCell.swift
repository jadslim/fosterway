//
//  NetworkCell.swift
//  fosterWayLogin
//
//  Created by Jad Slim on 2017-08-09.
//  Copyright © 2017 Jad Slim. All rights reserved.
//

import UIKit

class NetworkCell: UITableViewCell {
    var uid: String?
    var name: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
