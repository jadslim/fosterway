//
//  NetworkViewController.swift
//  fosterWayLogin
//
//  Created by Jad Slim on 2017-08-09.
//  Copyright © 2017 Jad Slim. All rights reserved.
//

import UIKit
import FirebaseDatabaseUI
import SwiftyJSON

class NetworkViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet weak var NetworkTableView: UITableView!
    
    var firebaseRef: DatabaseReference!
    var dataSource: FUITableViewDataSource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firebaseRef = Database.database().reference().child("user")
        NetworkTableView.delegate = self
        // Do any additional setup after loading the view.
        self.dataSource = self.NetworkTableView.bind(to: firebaseRef) { tableView, indexPath, snap in
            let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! NetworkCell
            let json = JSON((snap.value))
            if let FirstName = json["ProfileData"]["First Name"].string, let LastName = json["ProfileData"]["Last Name"].string {
                cell.textLabel?.text = FirstName.capitalized + " " + LastName.capitalized
                cell.name = FirstName.capitalized + " " + LastName.capitalized
            }
            cell.uid = snap.key
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected")
        if let cell = tableView.cellForRow(at: indexPath) as? NetworkCell {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "Profile") as! ProfileViewController
            controller.userID = cell.uid
            controller.FullName = cell.name
            self.show(controller, sender: nil)
        }
    }
}
