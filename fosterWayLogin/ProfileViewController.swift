//
//  ProfileViewController.swift
//  fosterWayLogin
//
//  Created by Jad Slim on 2017-08-08.
//  Copyright © 2017 Jad Slim. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON
import Kingfisher
import FirebaseDatabaseUI
class ProfileViewController: UIViewController {
    
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var ProfileImage: UIImageView!
    @IBOutlet weak var Province: UILabel!
    @IBOutlet weak var HighSchool: UILabel!
    @IBOutlet weak var ClassYear: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var Summary: UITextView!
    @IBOutlet weak var FollowButton: UIButton!
    @IBOutlet weak var MessageButton: UIButton!
    
    
    @IBOutlet weak var borderView: UIView!
    
    let defaults = UserDefaults.standard
    var userID: String?
    var ref: DatabaseReference!
    var followingStatus: Bool?
    var FullName: String?
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        roundedProfileImage(image: ProfileImage)
        self.observeProfileData()
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = UIColor.black.cgColor
        Summary.isSelectable = false
        Summary.isEditable = false
    }
    @objc func edit() {
        presentStoryViewController(storyboardID: "EditProfileController")
    }
    func roundedProfileImage(image: UIImageView) {
        image.layer.borderWidth = 1
        image.layer.masksToBounds = false
        image.layer.borderColor = UIColor.black.cgColor
        image.layer.cornerRadius = image.frame.height/2
        image.clipsToBounds = true
    }
    func observeProfileData() {
        configureProfile()
        var firstName = String()
        var lastName = String()
        ref.child("user").child(userID!).child("ProfileData").observe(DataEventType.value, with: { (snapshot) in
            print(snapshot.value)
            let json = JSON((snapshot.value))
//            if let FirstName = json["First Name"].string, let LastName = json["Last Name"].string {
//                firstName = FirstName
//                lastName = LastName
//                //self.Name.text = firstName + " " + lastName
//            }
            if let name = self.FullName {
                self.Name.text = name
            }
            self.Name.text = self.FullName
            if let HighSchool = json["High School"].string {
                if (HighSchool.isEmpty) {
                    self.HighSchool.text = "No High School Specified"
                } else {
                    self.HighSchool.text = HighSchool
                }
            }
            if let Province = json["Province"].string {
                if (Province.isEmpty) {
                    self.Province.text = "No Region Specified"
                } else {
                    self.Province.text = Province
                }
            }
            if let ClassYear = json["Class Year"].string {
                self.ClassYear.text = "Class of: " + ClassYear
            }
            if let Status = json["Status"].string {
                if (Status.isEmpty) {
                    self.status.text = "Status: No Status"
                } else {
                    self.status.text = "Status: " + Status
                }
            }
            if let Summary = json["Summary"].string {
                if (Summary.isEmpty) {
                    self.Summary.text = "Empty Summary"
                } else {
                    self.Summary.text = Summary
                }
            }
            if let ProfileImage = json["profileUrl"].string {
                if (ProfileImage.isEmpty) {
                } else {
                    self.ProfileImage.kf.indicatorType = .activity
                    self.ProfileImage.kf.setImage(with: URL(string:ProfileImage))
                }
            }
        }) { (error) in
            self.alert(message: error.localizedDescription)
        }
    }
    func configureProfile(){
        configureNav()
        if userID == (defaults.string(forKey: "uid"))!
        {
            FollowButton.isHidden = true
            MessageButton.isHidden = true
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(edit))
        } else {
            FollowButton.isHidden = false
            MessageButton.isHidden = false
            checkIfFollowing()
            self.navigationItem.rightBarButtonItem = nil
            //check if you're following
        }
    }
    func configureNav(){
  
                self.navigationController?.navigationBar.tintColor = UIColor.white
                self.navigationItem.title = "Profile"
    }

    @IBAction func FollowOrUnfollow(_ sender: Any) {
        let userRef = ref.child("user")

        if followingStatus == true {
            let childUpdates = ["/\(userID!)/followers/\((defaults.string(forKey: "uid"))!)": NSNull(),
                                "/\((defaults.string(forKey: "uid"))!)/followings/\(userID!)": NSNull()]
            userRef.updateChildValues(childUpdates, withCompletionBlock: { (error, ref) in
                if ((error) != nil) {
                    self.alert(message: "Error Following")
                } else {
                    self.configureFollowButton()
                    self.followingStatus = false
                }
            })
        } else {
        let childUpdates = ["/\(userID!)/followers/\((defaults.string(forKey: "uid"))!)": ServerValue.timestamp(),
                            "/\((defaults.string(forKey: "uid"))!)/followings/\(userID!)": ServerValue.timestamp()]
            userRef.updateChildValues(childUpdates, withCompletionBlock: { (error, ref) in
                if ((error) != nil) {
                    self.alert(message: "Error Unfollowing")
                } else {
                    self.configureUnfollowButton()
                    self.followingStatus = true
                }
            })
        }
        //if unfollowing
    }
    func checkIfFollowing(){
        ref.child("user").child((defaults.string(forKey: "uid"))!).child("followings").child(userID!).observeSingleEvent(of: .value) { (snapshot) in
            print(snapshot)
            if snapshot.exists() {
                self.configureUnfollowButton()
                self.followingStatus = true
            } else {
                self.configureFollowButton()
                self.followingStatus = false
            }
        }
    }
    func configureFollowButton() {
        FollowButton.setTitle("Follow", for: .normal)
        FollowButton.backgroundColor = UIColor(rgb: 0x6F99C8)
    }
    func configureUnfollowButton(){
        FollowButton.setTitle("Unfollow", for: .normal)
        FollowButton.backgroundColor = UIColor.gray
    }
    func signOut() {
        try! Auth.auth().signOut()
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func Message(_ sender: Any) {
        let uid = self.userID!
        let me = defaults.string(forKey: "uid")!
        print(uid)
        print(me)
        let reference = Database.database().reference().child("User-messages").child(me).child(uid).queryLimited(toLast: 50)
        reference.observeSingleEvent(of: .value, with: { [weak self] (snapshot) in
            let messages = Array(JSON(snapshot.value as Any).dictionaryValue.values).sorted(by: { (lhs, rhs) -> Bool in
                return lhs["date"].doubleValue < rhs["date"].doubleValue
            })
            let converted = self!.convertToChatItemProtocol(messages: messages)
            let chatlog = ChatLogController()
            chatlog.userUID = uid
            chatlog.otherName = self!.FullName!
            print(converted.count)
            chatlog.dataSource = DataSource(initialMessages: converted, uid: uid)
            chatlog.MessagesArray = FUIArray(query: Database.database().reference().child("User-messages").child(me).child(uid).queryLimited(toLast: 50), delegate: nil)
            self?.navigationController?.show(chatlog, sender: nil)

            
            
            
            
        })
        
        
    }
    deinit {
        activityIndicator.stopAnimating()
    }
}
