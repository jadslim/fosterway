//
//  VerificationCodeViewController.swift
//  fosterWayLogin
//
//  Created by Jad Slim on 2017-07-30.
//  Copyright © 2017 Jad Slim. All rights reserved.
//

import UIKit
import Firebase

class VerificationCodeViewController: UIViewController {
    let defaults = UserDefaults.standard
    var phoneNumber: String?
    var ref: DatabaseReference!
    @IBOutlet weak var signInOutlet: UIButton!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var VerificationCodeTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        navigationBarConfig()
        ref = Database.database().reference()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        phoneNumberLabel.text = phoneNumber
    }
    
    func navigationBarConfig(){
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem
    }
    func nextVerify() {
        startActivityIndicator()
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: defaults.string(forKey: "authVerificationID")!,
            verificationCode: VerificationCodeTextField.text!)
        print("outside auth")

        Auth.auth().signIn(with: credential) { (user, error) in
            if let error = error {
                self.alert(message: error.localizedDescription, title: "error")
                self.stopActivityIndicator()
                return
            }
            print("inside auth")
            //update firebase with uid
            self.updateFirebaseWithInfo()
            self.presentStoryViewController(storyboardID: "TabBar")
            self.stopActivityIndicator()
        }
    }
    func updateFirebaseWithInfo(){
        let userID = (Auth.auth().currentUser?.uid)!
        let userRef = ref.child("user").child(userID)
        let userInfo = ["uid": userID,
                    "phoneNumber": phoneNumber]
        //save in user defaults
        defaults.set(userID, forKey: "uid")
        userRef.updateChildValues(userInfo)
    }
    @IBAction func SignInAction(_ sender: Any) {
        nextVerify()
    }
    func startActivityIndicator() {
        ActivityIndicator.startAnimating()
        navigationItem.leftBarButtonItem?.isEnabled = false
        signInOutlet.isEnabled = false
    }
    
    func stopActivityIndicator(){
        ActivityIndicator.stopAnimating()
        navigationItem.leftBarButtonItem?.isEnabled = true
        signInOutlet.isEnabled = true
    }
}
