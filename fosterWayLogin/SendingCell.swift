//
//  SendingCell.swift
//  fosterWayLogin
//
//  Created by Rayan Slim on 2017-09-07.
//  Copyright © 2017 Jad Slim. All rights reserved.
//

import Foundation
import UIKit

class SendingStatusCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    var text: NSAttributedString? {
        didSet {
            self.label.attributedText = self.text
        }
    }
}
