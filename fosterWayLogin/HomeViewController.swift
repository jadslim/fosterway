//
//  HomeViewController.swift
//  
//
//  Created by Jad Slim on 2017-08-08.
//

import UIKit
import Firebase

class HomeViewController: UIViewController {
    
    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    let defaults = UserDefaults.standard
    var ref: DatabaseReference!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        syncData()
        ref = Database.database().reference()
        navigationBarConfig()
        checkIfProfileEdited()
    }

    func navigationBarConfig(){
        self.navigationItem.title = "Home"
        self.tabBarController?.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Profile", style: .plain, target: self, action: #selector(goToProfile))
        self.tabBarController?.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Signout", style: .plain, target: self, action: #selector(signOutaction))
        self.tabBarController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.tabBarController?.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
    }
    @objc func signOutaction(){
        self.signOut()
    }
    func BlockUI(){
        self.tabBarController?.navigationItem.rightBarButtonItem?.isEnabled = false
        self.tabBarController?.navigationItem.leftBarButtonItem?.isEnabled = false
        
        ActivityIndicator.startAnimating()
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        self.setTabBarVisible(visible: false, animated: false)
        setTabBarVisible(visible: isTabBarVisible, animated: true)
    }
    func UnblockUI() {
        self.tabBarController?.navigationItem.rightBarButtonItem?.isEnabled = true
        self.tabBarController?.navigationItem.leftBarButtonItem?.isEnabled = true
        ActivityIndicator.stopAnimating()
        setTabBarVisible(visible: !isTabBarVisible, animated: true)
    }
    @objc func goToProfile() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Profile") as! ProfileViewController
        controller.userID = (defaults.string(forKey: "uid"))!
        controller.FullName = Me.name
        self.show(controller, sender: nil)
    }
    func checkIfProfileEdited(){
        BlockUI()
        ref.child("user").child((defaults.string(forKey: "uid"))!).child("EditProfile").child("status").observeSingleEvent(of: .value) { (snapshot) in
            if let editProfileStatus = snapshot.value as? Bool {
                print(editProfileStatus)
                if editProfileStatus == true {
                    self.UnblockUI()
                    print("unblockUI")
                    //unblockUI
                } else {
                    //go to edit profile
                    self.goToEditProfile()
                }
            } else {
                self.goToEditProfile()
            }
        }
    }
    func goToEditProfile(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "EditProfileController") as! EditProfileViewController
        controller.cameFrom = "home"
        self.show(controller, sender: nil)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.UnblockUI()
        }
    }
    @objc func signOut() {
        try! Auth.auth().signOut()
        self.navigationController?.popToRootViewController(animated: true)
    }
}
extension UIViewController {
    
    func setTabBarVisible(visible: Bool, animated: Bool) {
        //* This cannot be called before viewDidLayoutSubviews(), because the frame is not set before this time
        
        // bail if the current state matches the desired state
        if (isTabBarVisible == visible) { return }
        
        // get a frame calculation ready
        let frame = self.tabBarController?.tabBar.frame
        let height = frame?.size.height
        let offsetY = (visible ? -height! : height)
        
        // zero duration means no animation
        let duration: TimeInterval = (animated ? 0.3 : 0.0)
        
        //  animate the tabBar
        if frame != nil {
            UIView.animate(withDuration: duration) {
                self.tabBarController?.tabBar.frame = frame!.offsetBy(dx: 0, dy: offsetY!)
                return
            }
        }
    }
    func syncData(){
        let scoresRef = Database.database().reference(withPath: "ontarioSchools")
        scoresRef.keepSynced(true)
    }
    var isTabBarVisible: Bool {
        return (self.tabBarController?.tabBar.frame.origin.y ?? 0) < self.view.frame.maxY
    }
}
