//
//  InputPhoneViewController.swift
//  fosterWayLogin
//
//  Created by Jad Slim on 2017-07-30.
//  Copyright © 2017 Jad Slim. All rights reserved.
//

import UIKit
import Firebase
import CountryPicker

class InputPhoneViewController: UIViewController, CountryPickerDelegate {
    var pickerView = CountryPicker()
    @IBOutlet weak var CountryTextField: UITextField!
    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    let defaults = UserDefaults.standard
    @IBOutlet weak var SendConCode: UIButton!
    
    
    var phoneCode: String?

    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        CountryTextField.text = phoneCode
        self.phoneCode = phoneCode
    }
    

    @IBOutlet weak var phoneNumberTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        pickerView.countryPickerDelegate = self
        CountryTextField.inputView = pickerView
        navigationBarConfig()
        CountryTextField.text = "+1"
        self.phoneCode = "+1"
    }
    
    func navigationBarConfig(){
        self.navigationItem.title = "Login"
        self.navigationItem.setHidesBackButton(true, animated:true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancel))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
    }
    @objc func cancel() {
        self.navigationController?.popViewController(animated: true)
    }
     func verify() {
        startActivityIndicator()
        guard let areaCode = self.phoneCode else {
            stopActivityIndicator()
            return
        }
        print(areaCode+phoneNumberTextField.text!)

        if !areaCode.isEmpty && !(phoneNumberTextField.text?.isEmpty)! {
        PhoneAuthProvider.provider().verifyPhoneNumber(areaCode + phoneNumberTextField.text!) { (verificationID, error) in
            if let error = error {
                self.stopActivityIndicator()
                self.alert(message: error.localizedDescription, title: "an error occured")
                return
            }
            // Sign in using the verificationID and the code sent to the user
            // ...
            let defaults = UserDefaults.standard
            defaults.set(verificationID, forKey: "authVerificationID")
            print("success loggin in")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "VerificationCodeViewController") as! VerificationCodeViewController
            controller.phoneNumber = areaCode + self.phoneNumberTextField.text!
            self.show(controller, sender: nil)
            self.stopActivityIndicator()
        }
        } else {
            stopActivityIndicator()
            self.alert(message: "Please Input Area Code and Phone Number", title: "error")
        }
    }
    
    @IBAction func SendConCodeAction(_ sender: Any) {
        self.verify()
    }
    func startActivityIndicator() {
        ActivityIndicator.startAnimating()
        SendConCode.isEnabled = false
        self.navigationItem.leftBarButtonItem?.isEnabled = false
    }
    
    func stopActivityIndicator(){
        ActivityIndicator.stopAnimating()
        SendConCode.isEnabled = true
        self.navigationItem.leftBarButtonItem?.isEnabled = true
    }
}
