//
//  ViewController.swift
//  fosterWayLogin
//
//  Created by Jad Slim on 2017-07-27.
//  Copyright © 2017 Jad Slim. All rights reserved.
//

import UIKit
import Firebase

class SignInViewController: UIViewController {

    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    let defaults = UserDefaults.standard

    override func viewDidLoad() {
        
        super.viewDidLoad()
        navigationBarConfig()
        self.startActivityIndicator()
            if Auth.auth().currentUser != nil {
                defaults.set(Auth.auth().currentUser?.uid, forKey: "uid")
                let when = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.presentStoryViewController(storyboardID: "TabBar")
                    let when = DispatchTime.now() + 1
                    DispatchQueue.main.asyncAfter(deadline: when) {
                    self.stopActivityIndicator()
                    }
                    print("logged in")
                }
            } else {
                    print("not logged in")
                    self.stopActivityIndicator()

            }
        
    }

    @IBAction func goToPhoneAuth(_ sender: Any) {
        self.presentStoryViewController(storyboardID: "InputPhoneViewController")
    }
    
    func startActivityIndicator() {
        ActivityIndicator.startAnimating()
    }
    
    func stopActivityIndicator(){
        ActivityIndicator.stopAnimating()
    }
    
    func navigationBarConfig(){
        self.navigationItem.title = "Welcome"
    }
}

