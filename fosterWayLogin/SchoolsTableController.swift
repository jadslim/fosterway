//
//  SchoolsTableController.swift
//  fosterWayLogin
//
//  Created by Jad Slim on 2017-08-21.
//  Copyright © 2017 Jad Slim. All rights reserved.
//

import UIKit
import FirebaseDatabaseUI
import SwiftyJSON

class SchoolsTableController: UIViewController, UITableViewDelegate, FUICollectionDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    var firebaseRef: DatabaseReference!
    var dataSource: FUITableViewDataSource!
    var delegate: schoolDelegate?
    var sorted: FUISortedArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Schools"
         sorted = FUISortedArray(query: Database.database().reference().child("ontarioSchools"), delegate: nil, sortDescriptor: { (lhs, rhs) -> ComparisonResult in
            return  (lhs.value as! String).compare(rhs.value as! String)
         })
        if let sorted = sorted {
        sorted.observeQuery()
        sorted.delegate = self
        } else {
            alert(message: "an error has occured")
        }
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "EditProfileController") as! EditProfileViewController
            delegate?.passSchool(school: (cell.textLabel?.text))
            self.navigationController?.popViewController(animated: true)
        }
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(self.sorted!.count)
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = (sorted![(UInt(indexPath.row))] as! DataSnapshot).value as! String
        return cell
    }
    func arrayDidBeginUpdates(_ collection: FUICollection) {
    self.blockUI()
    }
    func arrayDidEndUpdates(_ collection: FUICollection) {
        self.unblockUI()
    }
    func blockUI() {
        activity.startAnimating()
    }
    func unblockUI(){
        activity.stopAnimating()
    }
    func array(_ array: FUICollection, didAdd object: Any, at index: UInt) {

        self.tableView.insertRows(at: [IndexPath(row: Int(index), section: 0)], with: .automatic)
    }

    func array(_ array: FUICollection, didMove object: Any, from fromIndex: UInt, to toIndex: UInt) {

        self.tableView.insertRows(at: [IndexPath(row: Int(toIndex), section: 0)], with: .automatic)

        self.tableView.deleteRows(at: [IndexPath(row: Int(fromIndex), section: 0)], with: .automatic)
    }

    func array(_ array: FUICollection, didRemove object: Any, at index: UInt) {
        self.tableView.deleteRows(at: [IndexPath(row: Int(index), section: 0)], with: .automatic)
    }

    func array(_ array: FUICollection, didChange object: Any, at index: UInt) {
        self.tableView.reloadRows(at: [IndexPath(row: Int(index), section: 0)], with: .automatic)
    }

    
    
    func array(_ array: FUICollection, queryCancelledWithError error: Error) {
        fatalError()
    }
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
protocol schoolDelegate {
    func passSchool(school: String?)
}











