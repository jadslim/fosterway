//
//  ChatLogController.swift
//  fosterWayLogin
//
//  Created by Rayan Slim on 2017-09-07.
//  Copyright © 2017 Jad Slim. All rights reserved.
//

import UIKit
import Chatto
import ChattoAdditions
import FirebaseDatabaseUI
import SwiftyJSON
class ChatLogController: BaseChatViewController, FUICollectionDelegate {

    var presenter: BasicChatInputBarPresenter!
    var decorator = Decorator()
    var dataSource: DataSource!
    var userUID = String()
    var MessagesArray: FUIArray!
    var me = UserDefaults.standard.string(forKey: "uid")!
    var otherName = String()
    var _avatar: UIImage?
    
    override func createPresenterBuilders() -> [ChatItemType : [ChatItemPresenterBuilderProtocol]] {
        let textMessageBuilder = TextMessagePresenterBuilder(viewModelBuilder: TextBuilder(), interactionHandler: TextHandler())
        let chatColor = BaseMessageCollectionViewCellDefaultStyle.Colors(
            incoming: UIColor(hex: 0x8BD70C), // white background for incoming 0047A0
            outgoing: UIColor(hex: 0x0047A0) // black background for outgoing 8BD70C
        )
        let baseMessageStyle = avatar(colors: chatColor)
        
        let textStyle = TextMessageCollectionViewCellDefaultStyle.TextStyle(
            font: UIFont.systemFont(ofSize: 13),
            incomingColor: UIColor.black, // black text for incoming
            outgoingColor: UIColor.white, // white text for outgoing
            incomingInsets: UIEdgeInsets(top: 10, left: 19, bottom: 10, right: 15),
            outgoingInsets: UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 19)
        )
        let textCellStyle: TextMessageCollectionViewCellDefaultStyle = TextMessageCollectionViewCellDefaultStyle(
            textStyle: textStyle,
            baseStyle: baseMessageStyle)
        textMessageBuilder.baseMessageStyle = baseMessageStyle
        textMessageBuilder.textCellStyle = textCellStyle
        return [TextModel.chatItemType : [textMessageBuilder],
                SendingStatusModel.chatItemType: [SendingStatusPresenterBuilder()]]
    }
    override func createChatInputView() -> UIView {
//        let inputBar = Bundle.main.loadNibNamed("ChatInputBarcopy", owner: nil, options: nil)!.first as! ChatInputBar
        let inputBar = DemoChatInputBar.loadNib()
        var appearance = ChatInputBarAppearance()
        appearance.sendButtonAppearance.title = "Send"
        appearance.textInputAppearance.placeholderFont = UIFont.systemFont(ofSize: 15.0)
        appearance.sendButtonAppearance.font = UIFont.systemFont(ofSize: 15.0)
        appearance.textInputAppearance.font = UIFont.systemFont(ofSize: 15.0)
        appearance.sendButtonAppearance.titleColors = [UIControlStateWrapper(state: .normal): UIColor(hex: 0x0047A0)]
        appearance.textInputAppearance.placeholderText = "Type a message"
        self.presenter = BasicChatInputBarPresenter(chatInputBar: inputBar, chatInputItems: [handleSend()], chatInputBarAppearance: appearance)
        return inputBar
    }
    func handleSend() -> TextChatInputItem {
        let item = TextChatInputItem()
        item.textInputHandler = { text in
            
            let date = Date()
            let double = date.timeIntervalSinceReferenceDate
            let senderId = (UserDefaults.standard.string(forKey: "uid"))!
            let messageUID = ("\(double)" + senderId).replacingOccurrences(of: ".", with: "")
            let message = MessageModel(uid: messageUID, senderId: senderId, type: TextModel.chatItemType, isIncoming: false, date: date, status: .sending)
            let textMessage = TextModel(messageModel: message, text: text)
            self.dataSource.addMessage(message: textMessage)
            self.sendOnlineTextMessage(text: text, uid: messageUID, double: double, senderId: senderId)
            
        }
        return item
    }

    func sendOnlineTextMessage(text: String, uid: String, double: Double, senderId: String) {
        let message = ["text": text, "uid": uid, "date": double, "senderId": senderId, "status": "success", "type": TextModel.chatItemType] as [String : Any]
        let childUpdates = ["User-messages/\(senderId)/\(self.userUID)/\(uid)": message,
                            "User-messages/\(self.userUID)/\(senderId)/\(uid)": message,
                            "user/\(senderId)/Contacts/\(self.userUID)/lastMessage": message,
                            "user/\(self.userUID)/Contacts/\(senderId)/lastMessage": message,
                            "user/\(self.userUID)/Contacts/\(senderId)/name": Me.name,
                            "user/\(senderId)/Contacts/\(self.userUID)/name": self.otherName
            ] as [String : Any]

        Database.database().reference().updateChildValues(childUpdates) { [weak self] (error, _) in

            if error != nil {

                self?.dataSource.updateTextMessage(uid: uid, status: .failed)
                return
            }
            self?.dataSource.updateTextMessage(uid: uid, status: .success)

        }
        
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        self.chatDataSource = self.dataSource
        self.chatItemsDecorator = self.decorator
        self.MessagesArray.observeQuery()
        self.title = otherName

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.MessagesArray.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChatLogController {
    
    func array(_ array: FUICollection, didAdd object: Any, at index: UInt) {
        let message = JSON((object as! DataSnapshot).value as Any)
        let senderId = message["senderId"].stringValue
        let type = message["type"].stringValue
        let contains = self.dataSource.controller.items.contains { (collectionViewMessage) -> Bool in
            return collectionViewMessage.uid == message["uid"].stringValue
        }
        if contains == false {
            
            let model = MessageModel(uid: message["uid"].stringValue, senderId: senderId, type: type, isIncoming: senderId == me ? false : true, date: Date(timeIntervalSinceReferenceDate: message["date"].doubleValue), status: message["status"] == "success" ? MessageStatus.success : MessageStatus.sending)
                let textMessage = TextModel(messageModel: model, text: message["text"].stringValue)
                self.dataSource.addMessage(message: textMessage)
        }
    }
}

class avatar: BaseMessageCollectionViewCellDefaultStyle {
    
    override func avatarSize(viewModel: MessageViewModelProtocol) -> CGSize {
        return CGSize.zero
    }
}
