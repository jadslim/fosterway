//
//  DemoChatInputBar.swift
//  fosterWayLogin
//
//  Created by Rayan Slim on 2017-09-13.
//  Copyright © 2017 Jad Slim. All rights reserved.
//

import Foundation
import UIKit
import Chatto
import ChattoAdditions

class DemoChatInputBar : ChatInputBar {
    override class open func loadNib() -> ChatInputBar {
        let nibName = "DemoChatInputBar"
        let view = Bundle.main.loadNibNamed(nibName, owner: nil, options: nil)!.first as! ChatInputBar
        view.translatesAutoresizingMaskIntoConstraints = false
        view.frame = CGRect.zero
        return view
    }
}
class DemoExpandableTextView: ExpandableTextView{}
class DemoHorizontalStackScrollView: HorizontalStackScrollView {}
