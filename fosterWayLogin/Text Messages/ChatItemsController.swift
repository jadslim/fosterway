////
//  ChatItemsController.swift
//  MyChatApp
//
//  Created by Rayan Slim on 2017-06-19.
//  Copyright © 2017 Rayan Slim. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions
import FirebaseDatabase
import SwiftyJSON
class ChatItemsController: NSObject {
    
    var initialMessages = [ChatItemProtocol]()
    var items = [ChatItemProtocol]()
    var userUID: String!
    
    func loadIntoItemsArray(messagedNeeded: Int) {
        for index in stride(from: initialMessages.count - items.count, to: initialMessages.count - items.count - messagedNeeded, by: -1) {
            self.items.insert(initialMessages[index - 1], at: 0)
        }
    }
    
    
    func insertItem(message: ChatItemProtocol) {
        self.items.append(message)
    }
}

